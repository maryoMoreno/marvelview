import { Component, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { CharacterListComponent } from './character-list/character-list.component';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { CommonService } from './services/common.service';
import { BitacoraComponent } from './bitacora/bitacora.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet,
    MenuComponent,
    HeaderComponent,
    CharacterListComponent,
    BitacoraComponent
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  @Input() opcion: string = "";
  title = 'marvelView';
  characterList: any | undefined;

  constructor(private commonService: CommonService) { }

  ngOnInit(): void {
    this.commonService.opcion.subscribe((valor) => { this.opcion = valor });
    this.opcion="characterList";
    //this.getCharacterList()
  }


  getCharacterList() {
    console.log("-------------------------")
    fetch("http://localhost:8000/apiRest/character/all")
      .then((response) => response.json())
      .then((console.log))
      //.then((rs) => (this.characterList2 = rs));
  }

}
