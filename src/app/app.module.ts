import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ApiRestService } from './services/api-rest.service'; 

@NgModule({
  declarations: [ ],
  imports: [
    CommonModule,
    HttpClientModule,
    
  ],
  providers:[ApiRestService]
})
export class NgModuleModule { }
