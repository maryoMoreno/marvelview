import { Component, OnInit, inject, Input, Output, EventEmitter } from '@angular/core';
import { ApiRestService } from '../services/api-rest.service';
import { ModalService } from '../services/modal.service';
import { CommonService } from '../services/common.service';
import { CharacterDetalleComponent } from '../character-detalle/character-detalle.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-character-list',
  standalone: true,
  imports: [CharacterDetalleComponent],
  templateUrl: './character-list.component.html',
  styleUrl: './character-list.component.css'
})

export class CharacterListComponent {
  apiRest: ApiRestService = inject(ApiRestService);
  characterList: any | undefined;
  @Input() opcion: string = "";

  constructor(private modalService: ModalService, private commonService: CommonService) { }
  urlApi = "http://localhost:8000/apiRest";

  status = "";
  title = "Characters";
  character: any | undefined;
  showModal: boolean = false;

  ngOnInit(): void {
    this.commonService.modal.subscribe((valor) => { this.showModal = valor });
    this.commonService.opcion.subscribe((valor) => { this.opcion = valor });
    this.getCharacterList();
  }

  // ----- consulta por characterId -----
  getCharacter(characterId:number) {
    fetch(this.urlApi+"/character/byId/"+characterId)
      .then((response) => response.json())
      //.then(console.log)
      .then((rs) => (this.status = ""));
  }

  // ----- consulta todos los characters
  getCharacterList() {
    this.status = "Cargando ...";
    fetch(this.urlApi+"/character/all")
      .then((response) => response.json())
      .then((rs) => (this.characterList = rs.data.results))
      .then((rs) => (this.status = ""));
  }

  // ----- Cuandoi seleccionan un character -----
  characterSelected(id: number) {
    this.showModal = true;
    this.character = this.characterList[id];
    this.getCharacter(this.character.id)
  }
};





