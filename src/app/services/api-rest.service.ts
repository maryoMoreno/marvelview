import { Injectable, OnInit, EventEmitter } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { JsonPipe } from '@angular/common';
import { Comment } from '@angular/compiler';
import { CharacterListComponent } from '../character-list/character-list.component';

@Injectable({
  providedIn: 'root'
})

export class ApiRestService {
  protected userList: any[] = [{ 'id': 1, 'nombre': 'mario' }, { 'id': 2, 'nombre': 'juan' }];

  urlApi = "http://localhost:8000/apiRest";
  constructor() { }
  bitacoraList = new EventEmitter<any>();

  ngOnInit(): void {
  }

  data: any;
  getD() {
    return fetch(this.urlApi + "/character/all")
      .then((response) => response.json())
      //.then((quotesData) => (this.data = quotesData));
      .then(console.log);
  }

  async getCharactersList(): Promise<any> {
    var data = await fetch(this.urlApi + "/character/all");
    return await data.json() ?? {}
  }

  getS() {
  }

  async getBitacora(): Promise<any> {
    var rs = await fetch(this.urlApi + "/consulta/all");
    return await rs.json() ?? {}
  }

  getBitacora2() {
    return fetch(this.urlApi + "/consulta/all")
      .then((response) => response.json())
      .then(console.log)
      .then((quotesData) => (this.data = quotesData));
  }
  /*
    getCharacterList() :Observable<object> {
      var url = this.urlApi + "/character/all";
      console.log("url: "+url);
      return this.http.get(url);
    }
    */
}
