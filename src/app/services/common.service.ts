import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }
  modal = new EventEmitter<any>();
  opcion = new EventEmitter<string>();
  bitacoraList = new EventEmitter<any>();

}
