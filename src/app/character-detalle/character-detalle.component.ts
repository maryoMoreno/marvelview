import { Component, Input, EventEmitter, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalService } from '../services/modal.service';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-character-detalle',
  standalone: true,
  imports: [CommonModule, CharacterDetalleComponent],
  templateUrl: './character-detalle.component.html',
  styleUrl: './character-detalle.component.css'
})
export class CharacterDetalleComponent {

  constructor(private mdlService: ModalService, private commonService: CommonService) { }
  //visible: boolean=false;
  @Input() visible: boolean = false;
  @Input() character: any | undefined;

  characterSeleted(character: any) {
    //    alert(character);
    //    this.character = character;
  }

  openModal(show: boolean) {
    //  this.showModal=show;
  }

  closeModal() {
    //this.visible=false;
    this.commonService.modal.emit(false);
    this.commonService.opcion.emit("characterList")    
  }
}
