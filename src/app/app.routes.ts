import { Routes } from '@angular/router';
import { CharacterListComponent } from './character-list/character-list.component';
import { BitacoraComponent } from './bitacora/bitacora.component';

export const routes: Routes = [
    { path: 'characterList', component: CharacterListComponent },
    { path: 'bitacora', component: BitacoraComponent }
];
