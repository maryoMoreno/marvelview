import { Component, Output, EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BitacoraComponent } from '../bitacora/bitacora.component';
import { CharacterListComponent } from '../character-list/character-list.component';
import { CommonService } from '../services/common.service';
import { ApiRestService } from '../services/api-rest.service';

@Component({
  selector: 'app-menu',
  standalone: true,
  imports: [CommonModule, CharacterListComponent, BitacoraComponent],
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.css'
})
export class MenuComponent {
  @Output() sendOpcion = new EventEmitter<string>();
  @Output() sendBitacora = new EventEmitter<any>();

  urlApi = "http://localhost:8000/apiRest";

  constructor(private commonService: CommonService, private apiRest: ApiRestService) { }
  opcion: string = "characterList";
  bitacoraList: any ;

  ngOnInit(): void {
    this.commonService.opcion.subscribe((valor) => { this.opcion = valor });
    this.commonService.bitacoraList.subscribe((valor) => { this.bitacoraList = valor });
  }

  changeOpcion(opcion: string) {
    this.opcion = opcion;
    this.sendOpcion.emit(opcion);
    this.bitacoraList= this.getBitacora();
  }

  getBitacora() {
    fetch(this.urlApi + "/consulta/all")
      .then((response) => response.json())
      .then((rs) => (this.bitacoraList = rs.sort((a:any, b:any) => (a < b) ? -1 : 1)))
      .then(console.log);
      
  }
}
