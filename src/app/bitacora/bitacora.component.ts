import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonService } from '../services/common.service';
import { ApiRestService } from '../services/api-rest.service';
@Component({
  selector: 'app-bitacora',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './bitacora.component.html',
  styleUrl: './bitacora.component.css'
})
export class BitacoraComponent {
  @Input() opcion: string = "";
  @Input() bitacoraList:any |undefined;
  title = "Bitácora de consultas";
  constructor(private commonService: CommonService, private apiRest:ApiRestService ) { }
  ngOnInit(): void {
    this.commonService.opcion.subscribe((valor) => { this.opcion = valor });
    this.apiRest.bitacoraList.subscribe((valor) => { this.bitacoraList = valor });  
  }
}
